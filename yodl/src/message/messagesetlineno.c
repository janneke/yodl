#include "message.ih"

void message_setlineno(size_t line)
{
    m_message.d_lineno = line;
}
