#include "message.ih"

size_t message_lineno()
{
    return m_message.d_lineno;
}
