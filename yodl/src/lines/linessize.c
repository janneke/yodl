#include "lines.h"

size_t lines_size(register Lines *lines)
{
    return lines->d_size;
}
