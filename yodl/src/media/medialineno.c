#include "media.ih"

size_t media_lineno(register Media *mp)
{
    return mp->d_lineno;
}
